import os
import subprocess
import sys
from datetime import datetime

from dropbox import Dropbox
from dropbox.exceptions import AuthError, ApiError
from dropbox.files import WriteMode


class Backup:
    def __init__(self, name: str, path: str, time: datetime):
        self.name = name
        self.path = path
        self.time = time

    def get_file(self) -> str:
        return os.path.join(self.path, self.name)


class BackupManager:
    BACKUP_EXTENSION = 'sql'
    BACKUP_PATH = os.environ['BACKUP_PATH']
    BACKUP_NAME = 'dump'

    @classmethod
    def create_backup(cls, db_name: str, db_user: str, db_pass: str) -> Backup:
        time = datetime.now()
        name = cls.generate_file_name(time)
        path = cls.BACKUP_PATH

        args = ' '.join([
            'PGPASSWORD=' + db_pass,
            'pg_dump',
            '--no-acl',
            '--no-owner',
            '-U' + db_user,
            '-d' + db_name,
            '> ' + os.path.join(path, name)
        ])

        try:
            subprocess.run(args, check=True, shell=True)
        except subprocess.CalledProcessError as err:
            print(err)
            sys.exit()

        return Backup(name=name, path=path, time=time)

    @classmethod
    def generate_file_name(cls, time: datetime) -> str:
        timestamp = str(int(time.timestamp()))
        formatted_time = time.strftime('%Y-%m-%d_%H-%M-%S')
        return '.'.join([formatted_time, timestamp, cls.BACKUP_NAME, cls.BACKUP_EXTENSION])

    @classmethod
    def remove_backup(cls, backup: Backup):
        os.remove(backup.get_file())

    @classmethod
    def upload_to_dropbox(cls, backup: Backup):
        dbx = get_dropbox_client()

        with open(backup.get_file(), 'rb') as f:
            path = os.path.join('/', backup.time.strftime('%Y'), backup.time.strftime('%m'), backup.name)
            try:
                dbx.files_upload(f.read(), path, mode=WriteMode('overwrite'))
            except ApiError as err:
                # This checks for the specific error where a user doesn't have
                # enough Dropbox space quota to upload this file
                if err.error.is_path() and err.error.get_path().error.is_insufficient_space():
                    sys.exit('ERROR: Cannot upload insufficient space.')
                elif err.user_message_text:
                    print(err.user_message_text)
                    sys.exit()
                else:
                    print(err)
                    sys.exit()


def get_dropbox_client() -> Dropbox:
    # Create an instance of a Dropbox client.
    dbx = Dropbox(os.environ['DROPBOX_TOKEN'])

    # Check that the access token is valid
    try:
        dbx.users_get_current_account()
    except AuthError:
        sys.exit('ERROR: Invalid access token')

    return dbx


if __name__ == '__main__':
    print('Creating backup file...')
    backup = BackupManager.create_backup(
        db_name=os.environ['DB_NAME'],
        db_user=os.environ['DB_USER'],
        db_pass=os.environ['DB_PASS']
    )
    print('Backup file successfully created and temporary stored at ' + backup.get_file())

    print('Uploading backup file to Dropbox...')
    BackupManager.upload_to_dropbox(backup)
    print('Backup file successfully uploaded.')

    print('Removing temporary stored backup file...')
    BackupManager.remove_backup(backup)
    print('Temporary stored backup file successfully removed.')
