DB Dump
=======


Agreement
=========

- The `(.env)$` identifier of command line indicates that there must be active virtual environment.


Attention
=========

- Python support: `3.5.1`


Quick start guide
=================

Clone
-----

    $ git clone git@gitlab.com:efremov-dma/db_dump.git
    $ cd db_dump


Install virtualenv
------------------

    $ virtualenv .env
    $ source .env/bin/activate
    (.env)$


Install packages
----------------

    (.env)$ pip install -r requirements.txt


Set environment variables
-------------------------
    DB_NAME="database name"
    DB_USER="database user"
    DB_PASS="database password"
    DROPBOX_TOKEN="dropbox app access token" [see dropbox docs](https://dropbox.com/developers)


Run the project
----------------

    (.env)$ python db_dump.py
